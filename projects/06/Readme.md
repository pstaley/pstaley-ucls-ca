Project 6 HW:

The following assembler is incomplete. Modify the following
four methods:

1) dest()
2) comp()
3) jump()
4) to_binary()

so that the assembler prints out the right output. Right now,
for any input, the C-instructions are outputting:

'111accccccdddjjj', when we want something like:

'1110111111011011'

And the A-instructions are outputting:

'0vvvvvvvvvvvvvvv'

When we want something like:

'0000010101101000'

Modify those 4 methods to produce the correct output. Once
you're ready to test out your programs,
test each one at a time in the following
order (from simplest (symbol-less) to most challenging (contains symbols):

add.asm -> maxL.asm -> rectL.asm -> PongL.asm ->
Max.asm -> Rect.asm -> Pong.asm

You can use the following tools to check your own
assembler's output against the output of the correct
assembler:

- Debugger
- Assembler
- CPU Emulator
- Diffchecker (diffchecker.org)