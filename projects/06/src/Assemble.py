import sys

# Project 6 Instructions:
# The following program converts an ASM file

# C-instructions - 111accccccdddjjj

# A-instructions - 0vvvvvvvvvvvvvvv

# These dictionaries will hold the keys and values for the calculation, destination and jump portions:

# cccccc
comp_dictionary = dict()

# ddd
dest_dictionary = dict()

# jjj
jump_dictionary = dict()

# DO NOT MODIFY THESE DICTIONARIES
# Assemblers require predefined constants.
symbol_table = dict()
symbol_table['SP'] = 0
symbol_table['LCL'] = 1
symbol_table['ARG'] = 2
symbol_table['THIS'] = 3
symbol_table['THAT'] = 4
symbol_table['R0'] = 0
symbol_table['R1'] = 1
symbol_table['R2'] = 2
symbol_table['R3'] = 3
symbol_table['R4'] = 4
symbol_table['R5'] = 5
symbol_table['R6'] = 6
symbol_table['R7'] = 7
symbol_table['R8'] = 8
symbol_table['R9'] = 9
symbol_table['R10'] = 10
symbol_table['R11'] = 11
symbol_table['R12'] = 12
symbol_table['R13'] = 13
symbol_table['R14'] = 14
symbol_table['R15'] = 15
symbol_table['SCREEN'] = 16384
symbol_table['KBD'] = 24576

comp_dictionary['0'] = '0101010'
comp_dictionary['1'] = '0111111'
comp_dictionary['-1'] = '0111010'
comp_dictionary['D'] = '0001100'
comp_dictionary['A'] = '0110000'
comp_dictionary['!D'] = '0001101'
comp_dictionary['!A'] = '0110001'
comp_dictionary['-D'] = '0001111'
comp_dictionary['-A'] = '0110011'
comp_dictionary['D+1'] = '0011111'
comp_dictionary['A+1'] = '0110111'
comp_dictionary['D-1'] = '0001110'
comp_dictionary['A-1'] = '0110010'
comp_dictionary['D+A'] = '0000010'
comp_dictionary['D-A'] = '0010011'
comp_dictionary['A-D'] = '0000111'
comp_dictionary['D&A'] = '0000000'
comp_dictionary['D|A'] = '0010101'
comp_dictionary['M'] = '1110000'
comp_dictionary['!M'] = '1110001'
comp_dictionary['-M'] = '1110011'
comp_dictionary['M+1'] = '1110111'
comp_dictionary['M-1'] = '1110010'
comp_dictionary['D+M'] = '1000010'
comp_dictionary['D-M'] = '1010011'
comp_dictionary['M-D'] = '1000111'
comp_dictionary['D&M'] = '1000000'
comp_dictionary['D|M'] = '1010101'

dest_dictionary['0'] = '000'
dest_dictionary['null'] = '000'
dest_dictionary['M'] = '001'
dest_dictionary['D'] = '010'
dest_dictionary['MD'] = '011'
dest_dictionary['A'] = '100'
dest_dictionary['AM'] = '101'
dest_dictionary['AD'] = '110'
dest_dictionary['AMD'] = '111'

jump_dictionary['null'] = '000'
jump_dictionary['JGT'] = '001'
jump_dictionary['JEQ'] = '010'
jump_dictionary['JGE'] = '011'
jump_dictionary['JLT'] = '100'
jump_dictionary['JNE'] = '101'
jump_dictionary['JLE'] = '110'
jump_dictionary['JMP'] = '111'

a_command = "A_COMMAND"
c_command = "C_COMMAND"
l_command = "L_COMMAND"


# DO NOT MODIFY THE OPERATE() METHOD
def operate(file):
    lines = file.readlines()
    comment_less_lines = []
    symbol_less_lines = []
    skipped_lines = 0

    # pass 1: decommenting and de-whitespacing
    for line_number, line in enumerate(lines):
        line = ''.join(line.split("//")[0].split())
        if line != "":
            comment_less_lines.append(line)

    # pass 2: adding labels to symbol table
    for line_number, line in enumerate(comment_less_lines):
        if (line.startswith("(")):
            symbol_table[(line.split("(")[1].split(")")[0])] = line_number - skipped_lines
            skipped_lines += 1
        else:
            symbol_less_lines.append(line)

    # pass 3: translating lines
    for line_number, line in enumerate(symbol_less_lines):
        translation = translate(line)
        output_file.write(translation)
        output_file.write('\n')


# Takes in a given line, returns the A or C instruction in a 16 digit bitstring.
# DO NOT MODIFY THE TRANSLATE METHOD
def translate(line: str):
    global variableCounter
    instruction = []

    # If the line an A-instruction, do work for an A-instruction.
    if command_type(line) == a_command:
        instruction.append('0')
        if (symbol(line).isdigit()):
            instruction.append(to_binary(int(symbol(line))))
        else:
            if symbol(line) in symbol_table:
                instruction.append(to_binary(symbol_table[symbol(line)]))
            else:
                symbol_table[symbol(line)] = variableCounter
                variableCounter += 1
                instruction.append(to_binary(symbol_table[symbol(line)]))

    # If the line is a C-instruction, do work for a C-instruction
    elif command_type(line) == c_command:
        instruction.append('111')
        instruction.append(comp(line))
        instruction.append(dest(line))
        instruction.append(jump(line))
    return ''.join(instruction)


# DO NOT MODIFY THIS COMMAND_TYPE METHOD
def command_type(line: str):
    if line.startswith("@"):
        return a_command
    else:
        return c_command


# DO NOT MODIFY THIS SYMBOL() METHOD
def symbol(line: str):
    return line.split("@")[1]


# DO NOT MODIFY ABOVE THIS
# ==========================================================================================================

# HW PART 1)
# The dest() takes in a line, such as A=M, and returns
# the 3 digit ddd part of the C-instruction.

# Example: inputting 'A=M' returns '100'.
# TO DO: Write out the method that returns the 3-digit 'ddd' bit string for a given line.
# Right now it returns 'ddd'. Change it so it outputs the correct bitstring.
def dest(line: str):
    # CHANGE ME:
    if ("=" in line):
        return dest_dictionary[(line.split("="))[0].split(";")[0]]
    else:
        return dest_dictionary['null']


# HW PART 2)
# The comp() takes in a line, such as A=M, and returns
# the 7 digit acccccc part of the C-instruction.
# This includes both the A/M part of the ALU calculation and the computation portion.
# Both the A/M as inputs are already accounted for in the computation dictionary.

# Example: inputting 'A=M' returns '1110000'.
# TO DO: Write out the method that returns the 7-digit 'acccccc' bit string for a given line.
# Right now it returns 'acccccc'. Change it so it outputs the correct 7-digit bitstring.

def comp(line: str):
    # CHANGE ME
    if ("=" in line):
        return comp_dictionary[(line.split("=")[1]).split(";")[0]]
    else:
        return comp_dictionary[line.split(";")[0]]


# HW PART 3)
# The jump() function takes in a line, such as A=M; JMP, and returns
# the 3 digit jjj part of the C-instruction.

# Example: inputting 'A=M;JMP' returns '111'.
# TO DO: Write out the method that returns the 3-digit 'jjj' bit string for a given line.
# Right now it returns 'jjj'. Change it so it outputs the correct bitstring.

def jump(line: str):
    if (len(line.split(";")) > 1):
            return jump_dictionary[line.split(";")[1]]
    else:
            return jump_dictionary['null']



# HW PART 4 (last part):

# The following function takes in a positive integer, and should return the binary 15-digit bit-string to
# be appended to form an A-instruction.

# Change the method so that it actually outputs a 15-digit binary bitstring.

# Right now, for all input, a 15-digit 'vvvvvvvvvvvvvvv' is returned. change it so that for a given
# input, the correct value is returned.
#
# For example: n=3 ==> 000000000000011
def n_tobin(n):
    return int(bin(n)[2:])


def to_binary(n: int):
    return ('000000000000000' + str(n_tobin(n)))[-15:]


# ==========================================================================================================
# DO NOT MODIFY BELOW THIS
variableCounter = 16

# Work through all the files, to build up your program.
# Test each one at a time in the following order (from simplest to most challenging):
# add.asm -> maxL.asm -> rectL.asm -> PongL.asm -> Max.asm -> Rect.asm -> Pong.asm

# Use the diffchecker to compare correct output from the Nand2Tetris's assembler against the output that you
# produce in your own program.

# You are finished when Pong.asm works.
file = "06/pong/Pong.asm"
input_file = open(file, "r")
output_file = open(file.split(".")[0] + ".hack", "w")
operate(input_file)
