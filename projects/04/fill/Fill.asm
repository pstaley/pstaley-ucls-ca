// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

(STEADY)

	@16384		//limit is at 24574
	D=A
	@pxaddr
	M=D
	(WFILL)		//WHITE FILL
		@pxaddr
		A=M
		M=0
		@pxaddr
		M=M+1
		@24575
		D=A
		@pxaddr
		D=M-D
		@WFILL
		D;JLE


	@24576		//CK KEYPRESS
	A=M
	D=A
	@STEADY
	D;JEQ
@16384		//limit is at 24384
D=A
@pxaddr
M=D
(FILL)
	@pxaddr
	A=M
	M=-1
	@pxaddr
	M=M+1
	@24575
	D=A
	@pxaddr
	D=M-D
	@FILL
	D;JLE
(FREEZE)
	@24576		//CK KEYPRESS
	A=M
	D=A
	@FREEZE 
	D;JNE

@STEADY
0;JEQ




